/**
 ** This file is part of the Project qconv.
 ** Copyright 2018 Mihai Niculescu <i.mihai.niculescu@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <cstdlib> // for getenv()

#include <boost/format.hpp>

#include "fileprocessor.h"

#include <iostream>

batchprocessor::batch_processing_list batchprocessor::generate_from_environment(int argc, char *argv[])
{
    batch_processing_list fileList;

    if(argc<3) return fileList;

    // parsed from parameters
    uint64_t startIndex = std::stoi(argv[1]);
    uint64_t endIndex = std::stoi(argv[2]);

    // parsed from environment
//    std::cout << " INPUT LINE: " <<std::getenv("basefilename") << " " << std::getenv("datadir") << " " << std::getenv("outputdir") << std::endl;

    const char* baseFileName_carr = std::getenv("basefilename");
    const char* datadir_carr = std::getenv("datadir");
    const char* outputdir_carr = std::getenv("outputdir");

    // if we get here by direct execution of the application or environment didn't set these variables
    if(!baseFileName_carr || !datadir_carr || !outputdir_carr)
        return fileList;

    std::string baseFileName(baseFileName_carr);
    std::string datadir(datadir_carr);
    std::string outputdir(outputdir_carr);

    if(baseFileName.empty() || datadir.empty() || outputdir.empty())
        return fileList;

//    std::cout <<  " CHECKED LINE: " << baseFileName << " " << datadir << " " << outputdir << std::endl;

    if(datadir.back()!='/') datadir.push_back('/');
    if(outputdir.back()!='/') outputdir.push_back('/');

    // fill up the list
    for(int i=startIndex; i<=endIndex; ++i){
        batch_item item;
        boost::format fmt(baseFileName); // base file name is unformated
        fmt = fmt % i;
        std::string curfileName = fmt.str();
        item.filename = datadir + curfileName;
        item.outputfilename = outputdir + curfileName;
        item.processExitStatus = false;

        fileList.push_back(item);
    }

    return fileList;
}
