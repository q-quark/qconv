/**
 ** This file is part of the Project qconv.
 ** Copyright 2018 Mihai Niculescu <i.mihai.niculescu@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef FILEPROCESSOR_H
#define FILEPROCESSOR_H

#include <string>
#include <vector>
#include <ostream>

namespace batchprocessor {


struct batch_item
{
    std::string filename;
    std::string outputfilename; // base file name for output files
    std::vector<std::string> outputfiles; // register here all output files
    bool processExitStatus;
};

using batch_processing_list = std::vector<batch_item>;


batch_processing_list generate_from_environment(int argc, char *argv[]);

} // ns fileprocessor

inline std::ostream& operator<<(std::ostream& ost, const batchprocessor::batch_item& i){

    ost << "( batch-item filename: " << i.filename << " outputfilename: " << i.outputfilename << "\n"
        << " list-of-outputfiles: size(" << i.outputfiles.size() << ")\n ";
    for(const auto& e : i.outputfiles){
        ost << "outputfile:" << e << "\n";
    }
    ost << " exitstatus: " << i.processExitStatus
        << "\n) ";


    return ost;
}

inline std::ostream& operator<<(std::ostream& ost, const batchprocessor::batch_processing_list& l){

    ost << "FileList: " << l.size() << " elements\n";
    for(const auto& e : l){
        ost << e << "\n";
    }

    return ost;
}

#endif // FILEPROCESSOR_H
