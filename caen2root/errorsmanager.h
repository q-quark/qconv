/**
 ** This file is part of the Project qconv.
 ** Copyright 2018 Mihai Niculescu <i.mihai.niculescu@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef ERRORSMANAGER_H
#define ERRORSMANAGER_H

#include <string>
#include <ostream>
#include <map>
#include <vector>

class Errors_Mngr{
    friend std::ostream& operator<<(std::ostream&, const Errors_Mngr&);
public:
    Errors_Mngr() { }

    enum CAEN_READ_ERROR {
        CAEN_READ_ENERGY,
        CAEN_READ_QSHORT
    };

    void add_error(int eventid, Errors_Mngr::CAEN_READ_ERROR err){
        gErrorsMap[eventid].push_back(err);
    }

    static std::string get_error_str(CAEN_READ_ERROR e){
        switch (e) {
        case CAEN_READ_ERROR::CAEN_READ_ENERGY:
            return "Energy expected!";
            break;
        case CAEN_READ_ERROR::CAEN_READ_QSHORT:
            return "QShort expected!";
            break;
        default:
            return "Unknown error!";
            break;
        }
    }

    void write_for_event(std::ostream& ost, int eventid) const {
        const std::vector<CAEN_READ_ERROR> errList = gErrorsMap.at(eventid);

        for(const auto& e : errList){
            ost << get_error_str(e);
        }
    }

    bool hasErrors(int eventid) const {
        return gErrorsMap.find(eventid)!=gErrorsMap.end();
    }

    uint count() const {
        return gErrorsMap.size();
    }

private:
    std::map<int,std::vector<Errors_Mngr::CAEN_READ_ERROR>> gErrorsMap; // maps event number to the list of its errors
};


inline std::ostream& operator<<(std::ostream& ost, const Errors_Mngr& errmngr){
    for(const auto& kv : errmngr.gErrorsMap){
        ost << "Event: " << kv.first << "\n";
        for(const auto& e : kv.second){
            ost << Errors_Mngr::get_error_str(e);
        }
    }

    return ost;
}

#endif // ERRORSMANAGER_H
