/**
 ** This file is part of the Project qconv.
 ** Copyright 2018:
 **     Mihai Niculescu <i.mihai.niculescu@gmail.com>
 **     Dragos Nichita <dragos.nichita@ndf.ro>
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <cstdlib>

// boost libs (required by uncompress function)
#include <boost/filesystem.hpp>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/gzip.hpp>

// ROOT libs
#include <TFile.h>
#include <TTree.h>

#include "errorsmanager.h"
#include "fileprocessor.h"

#define BITS_OF_A_NIBBLE 4

void process(const batchprocessor::batch_item& item);

int main(int argc, char *argv[])
{
    batchprocessor::batch_processing_list fileList = batchprocessor::generate_from_environment(argc,argv);

    std::cout << "Processing " << fileList << std::endl;

    std::for_each(std::begin(fileList), std::end(fileList), process);

    std::cout << "\n\n==================== Task DONE ====================\n";
    std::cout << "\tprocessed " << fileList.size() << " files" << std::endl;

    return 0;
}

//----------------------------------------------------------

inline bool uncompress_archive_to (const std::string& archive, const std::string& outputfile)
{
    using namespace std;
    using namespace boost::iostreams;

    try {
        ifstream file(archive, ios_base::in | ios_base::binary);
        filtering_streambuf<input> in;
        in.push(gzip_decompressor());
        in.push(file);

        ofstream outfile(outputfile, ios_base::out | ios_base::binary);
        boost::iostreams::copy(in, outfile);
    }
    catch(...){
        std::cout << "uncompressing archive " << archive << " failed! " << std::endl;
        return false;
    }

    return true;
}

std::ostream& operator<<(std::ostream& ost, const std::vector<uint32_t>& buf){

    for(const auto& e : buf){
        ost << std::hex << e << "\n";
    }

    return ost;
}

void process(const batchprocessor::batch_item &item)
{
    int runno=1;
    int evno, tr, modno, chno;
    double timestamp, energy, qs;
    std::vector<uint32_t> buffer(6, 0);
    std::vector<uint32_t> tempb(4,0);

    std::cout << "\nProcessing file:" << item.filename  << std::endl;
    Errors_Mngr errMngr;

    std::string archiveFileName(item.filename); // the GZip archive
    std::string datafile(item.outputfilename + ".raw"); // the extracted file from the archive
    std::string outputfile(item.outputfilename + ".root"); // the output ROOT file

    std::cout << "\nUncompressing archive " << archiveFileName << " => " << datafile << std::endl;
    std::cout << "outputfile " << outputfile << std::endl;
    if(uncompress_archive_to(archiveFileName, datafile)){

        std::ifstream fin(datafile, std::ios::in | std::ios::binary);
        if (!fin) {
            std::cout << "Error : Input Data file not found!" << std::endl;
            return;
        }

        TFile *rootfile = new TFile(outputfile.c_str(),"RECREATE");
        TTree* FissData = new TTree("FissData", "FissData");
        FissData->Branch("RunNumber",&runno,"runno/I");
        FissData->Branch("EventNumber",&evno,"evno/I");
        FissData->Branch("TimeReset",&tr,"tr/I");
        FissData->Branch("TimeStamp",&timestamp,"timestamp/D");
        FissData->Branch("ModuleNumber",&modno,"modno/I");
        FissData->Branch("ChannelNumber",&chno,"chno/I");
        FissData->Branch("Energy",&energy,"energy/D");
        FissData->Branch("Qshort",&qs,"qs/D");

        std::cout << "Processing file:" << datafile << "\n";
        // register output file name
        // item.outputfiles.push_back(outputfile);

        uint64_t curEventId = 0;
        while (++curEventId, fin.read(reinterpret_cast<char *>(&buffer[0]), buffer.size() * 4))
        {
            static uint checkHeader;
            checkHeader = ((buffer[0] & 0x00FF000000) >> (6*BITS_OF_A_NIBBLE));
            if ( checkHeader == 94 )
            {
                fin.read(reinterpret_cast<char *>(&tempb[0]), tempb.size() * 4);
                continue;
            }

            if ( checkHeader == 69 ) // is Header
            {
                // std::cout << "Parsed header:" << buffer;
                continue;
            }
            else
            {
                //                    if (curEventId % 10000 == 0) std::cout << "Event " << std::dec << curEventId<< std::endl;
                evno = curEventId;
                modno = (buffer[0] & 0x000F000000) >> (6*BITS_OF_A_NIBBLE);
                tr = (buffer[0] & 0x00000FFFFF) >> (0*BITS_OF_A_NIBBLE);

                timestamp = buffer[1];

                chno = (buffer[2] & 0x00000F0000) >> (4*BITS_OF_A_NIBBLE);
                if (((buffer[2] & 0x0000F00000) >> (5*BITS_OF_A_NIBBLE)) != 0 ) errMngr.add_error(curEventId, Errors_Mngr::CAEN_READ_ENERGY);

                //--------------------
                energy = (buffer[2] & 0x000000FFFF) >> (0*BITS_OF_A_NIBBLE);

                if (((buffer[4] & 0x0000F00000) >> (5*BITS_OF_A_NIBBLE)) != 4 ) errMngr.add_error(curEventId, Errors_Mngr::CAEN_READ_QSHORT);

                //---------------------
                qs = (buffer[4] & 0x000000FFFF) >> (0*BITS_OF_A_NIBBLE);

                //-----------------------

                FissData->Fill();
            }

            // if current event has reported errors
            if (errMngr.hasErrors(curEventId))
                std::cout << buffer;

        } // while()


        rootfile->Write();
        rootfile->Close();
        std::cout << "Data saved to " << outputfile << std::endl;

        delete rootfile;

    } // if decompressed from archive

    // remove the data file (uncompressed binary file)
    std::cout << "Removing datafile " << datafile << std::endl;
    boost::filesystem::remove(datafile);

    // register the file was processed OK
    //    item.processExitStatus = true;

    std::cout << " File DONE with " << errMngr.count() << " events have errors!\n" << std::endl ;
    std::cout << errMngr;

    std::cout.flush();
}
