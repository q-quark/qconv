/**
 ** This file is part of the Project qconv.
 ** Copyright 2018 Mihai Niculescu <i.mihai.niculescu@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef CONVERTERMANAGER_H
#define CONVERTERMANAGER_H

#include <string>
#include <system_error>
#include <vector>

enum class ConverterState {
    Unknown,
    Started,
    Running,
    Stopped,
    Finished
};
 
class ConverterManager
{
public:
    ConverterManager();
    ConverterManager(const std::string& filename, uint64_t startId, uint64_t endId);
    ConverterManager(uint64_t startId, uint64_t endId);
    ~ConverterManager() = default;

    std::string baseFileName() const;
    void setBaseFileName(const std::string &baseFileName);

    std::string datadir() const;
    void setDatadir(const std::string &datadir);

    std::string outputdir() const;
    void setOutputdir(const std::string &outputdir);

    void setRange(uint64_t startId, uint64_t endId);
    std::string error_text() const;

    void run(uint32_t njobs=1); // run with this number of threads

    std::string converter() const;
    void setConverter(const std::string &converter);

private:
    struct ProcStatus {
        std::error_code err;
        std::string cmd;
    };

    void report_error(const std::string& text); // throws exception
    ConverterState m_state;

    // input members
    std::string m_baseFileName; // a formated name for the input file
    std::string m_converter; // name of the converter to run on the file
    uint64_t m_startFileNumber{0};
    uint64_t m_endFileNumber{0};
    std::string m_datadir;
    std::string m_outputdir;

    // computed members
    std::string m_errText; // text if an error occured
    uint m_chunk_length{0}; // number of files to process per thread
    uint m_remaining_chunk{0}; // unassigned number of files
    std::vector<ProcStatus> m_procStats;
};

#endif // CONVERTERMANAGER_H
