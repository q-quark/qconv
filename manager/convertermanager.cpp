/**
 ** This file is part of the Project qconv.
 ** Copyright 2018 Mihai Niculescu <i.mihai.niculescu@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <exception>
#include <cmath>
#include <iostream>

// boost libs
#include <boost/process.hpp>

// other
#include "convertermanager.h"

ConverterManager::ConverterManager()
    : m_state{ConverterState::Unknown}
{

}

ConverterManager::ConverterManager(const std::string &filename, uint64_t startId, uint64_t endId)
    : m_state{ConverterState::Unknown},
      m_baseFileName(filename),
      m_startFileNumber(startId),
      m_endFileNumber(endId)
{

}

ConverterManager::ConverterManager(uint64_t startId, uint64_t endId)
    : m_state{ConverterState::Unknown},
      m_startFileNumber(startId),
      m_endFileNumber(endId)
{

}

std::string ConverterManager::datadir() const
{
    return m_datadir;
}

void ConverterManager::setDatadir(const std::string &datadir)
{
    m_datadir = datadir;
}

std::string ConverterManager::outputdir() const
{
    return m_outputdir;
}

void ConverterManager::setOutputdir(const std::string &outputdir)
{
    m_outputdir = outputdir;
}

void ConverterManager::setRange(uint64_t startId, uint64_t endId)
{
    m_startFileNumber   = startId;
    m_endFileNumber     = endId;
}

std::string ConverterManager::error_text() const
{
    return m_errText;
}

void ConverterManager::report_error(const std::string &text)
{
    m_state = ConverterState::Finished;
    m_errText = text;
    throw std::invalid_argument(text);
}

std::string ConverterManager::converter() const
{
    return m_converter;
}

void ConverterManager::setConverter(const std::string &converter)
{
    m_converter = converter;
}

std::string ConverterManager::baseFileName() const
{
    return m_baseFileName;
}

void ConverterManager::setBaseFileName(const std::string &baseFileName)
{
    m_baseFileName = baseFileName;
}

void ConverterManager::run(uint32_t njobs)
{
    if(m_state!=ConverterState::Unknown)
        return; // stop if already running

    // do some checks
    if(njobs==0) report_error("Invalid Number of processs. Must be greater than 0");
    if(m_datadir.empty()) report_error("Invalid datadir");
    if(m_outputdir.empty()) report_error("Invalid outputdir");

    // lets start
    m_state = ConverterState::Started;

    // lets estimate number of files to process per process
    float number_of_chunks = float(m_endFileNumber - m_startFileNumber)/njobs;
    m_chunk_length = std::trunc(number_of_chunks); // number of files per process
    m_remaining_chunk = number_of_chunks - m_chunk_length; // number of files not assigned to any process

    // lets prepare the child processes
    namespace bp = boost::process;

    try {
        // share some info with the child processes
        auto env = boost::this_process::environment(); //get a handle to the current environment
        bp::environment child_env = env; //copy it into a environment separate to the one of this process
        child_env["basefilename"]=m_baseFileName;
        child_env["datadir"]=m_datadir;
        child_env["outputdir"]=m_outputdir;

        // lets launch the child processes
        uint s = m_startFileNumber;
        uint e = s + m_chunk_length;
        m_procStats.resize(njobs);

        m_state = ConverterState::Running;

        for(int i=0; i<njobs; ++i){ //! FIXME: warning number of remaning chunks!!!
            // command to run
            m_procStats[i].cmd = m_converter + " " + std::to_string(s) + " " + std::to_string(e);
            std::string child_stdout(m_outputdir + m_baseFileName + "_" + std::to_string(s) + "-" + std::to_string(e) + "_out.log");
            std::string child_stderr(m_outputdir + m_baseFileName + "_" + std::to_string(s) + "-" + std::to_string(e) + "_err.log");
            
            std::cout << "Starting Detached Process" << i << " cmd: [" << m_procStats[i].cmd << "].." << std::endl;
            
            bp::spawn(bp::search_path(m_converter), std::to_string(s), std::to_string(e), bp::std_out > child_stdout, bp::std_err > child_stderr, child_env);
            s = e +1;
            e = std::min(s + m_chunk_length, (uint)m_endFileNumber);
        }



    }
    catch(bp::process_error err)
    {
        std::cout << "processes didn't end well : "<< err.what() << std::endl;
    }
    catch(...)
    {
        std::cout << "processes didn't end well : unhandled error" << std::endl;
    }
    
    m_state = ConverterState::Finished;
}

