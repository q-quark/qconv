/**
 ** This file is part of the Project qconv.
 ** Copyright 2018 Mihai Niculescu <i.mihai.niculescu@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef SETTINGS_H
#define SETTINGS_H

#include <string>
#include <ostream>

// boost libs
#include <boost/optional.hpp>

#define APP_NAME "qconv"
#define CONVERTER_VERSION 0.1

class Settings
{
    friend std::ostream& operator<<(std::ostream&, const Settings&);
public:
    static boost::optional<Settings> parse(int argc, char* argv[]);

    std::string convertername() const;
    std::string basefilename() const;
    std::string datadir() const;
    std::string outputdir() const;
    uint64_t ibegin() const;
    uint64_t iend() const;
    uint32_t njobs() const;

    std::ostream& print_info(std::ostream& ost) const;

private:
    Settings();

    std::string m_convertername;
    std::string m_basefilename;
    std::string m_datadir;
    std::string m_outputdir;
    std::string m_configfile;
    uint64_t m_ibegin;
    uint64_t m_iend;
    uint32_t m_njobs;
};
#endif // SETTINGS_H
