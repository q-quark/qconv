/**
 ** This file is part of the Project qconv.
 ** Copyright 2018 Mihai Niculescu <i.mihai.niculescu@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/


// c++14 libs
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <fstream>
#include <iostream>

// boost libs
#include <boost/optional.hpp>

#include "convertermanager.h"
#include "settings.h"


int main(int argc, char* argv[])
{
    auto settings = Settings::parse(argc,argv);

    // if we couldn't get settings from arguments
    if(!settings) // this can happen when passing the arguments --help or --version
        return 0;

    // Rewrite this when using c++17
    // we need to do it this way because boost::optional ..
    //has some issues with overloading output operator
    std::cout << "Running with configuration:\n";
    settings->print_info(std::cout);
    std::cout << std::endl;

    ConverterManager conv(settings->ibegin(), settings->iend());
    conv.setConverter(settings->convertername() );
    conv.setBaseFileName(settings->basefilename());
    conv.setDatadir(settings->datadir());
    conv.setOutputdir(settings->outputdir());
    conv.run(settings->njobs());

    return 0;
}
