/**
 ** This file is part of the Project qconv.
 ** Copyright 2018 Mihai Niculescu <i.mihai.niculescu@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <iostream>

// boost libs
#include <boost/program_options.hpp>

#include "settings.h"


Settings::Settings()
{

}

boost::optional<Settings> Settings::parse(int argc, char *argv[])
{
    namespace po = boost::program_options;


    // Declare an options description instance which will include
    // all the options
    po::options_description all_opts("Allowed options");
    po::variables_map vm;
    Settings result;

    try {
        // Declare three groups of option
        po::options_description general_opts("Generic options");
        general_opts.add_options()
                ("help,h", "shows this help and exit")
                ("version,v", "output version information and exit")
                ("config", po::value<std::string>(&result.m_configfile), "load configuration from this file.")
                ;

        po::options_description processmgr_opts("Process Manager options");
        processmgr_opts.add_options()
                ("converter,c", po::value<std::string>(&result.m_convertername)->required(), "name of the converter executable")
                ("filename,f", po::value<std::string>(&result.m_basefilename)->default_value("data_%1%.gz"), "a formated string of the filename to process")
                ("data-dir,i", po::value<std::string>(&result.m_datadir)->required(), "path to the data directory")
                ("output-dir,o", po::value<std::string>(&result.m_outputdir)->default_value("/tmp"), "path to the output directory")
                ("begin-index,b", po::value<uint64_t>(&result.m_ibegin)->default_value(0), "starting index of the file")
                ("end-index,e", po::value<uint64_t>(&result.m_iend)->required(), "end index of the file")
                ("num-jobs,j", po::value<uint32_t>(&result.m_njobs)->default_value(1), "number of jobs(processes/threads) to spawn")
                ;

        all_opts.add(general_opts).add(processmgr_opts);

        po::store(po::parse_command_line(argc, argv, all_opts), vm);

        if (vm.count("help"))
        {
            std::cout << APP_NAME
                      << " - A Batch processing application."
                      << "\n\tLaunches multiple child processes in order to process files in parallel\n\n"
                      << all_opts;
            return boost::none;
        }

        if (vm.count("version"))
        {
            std::cout << APP_NAME << " " << CONVERTER_VERSION
                      << "\nCopyright (C) 2018"
                         "\nLicense GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>."
                         "\nThis is free software: you are free to change and redistribute it."
                         "\nThere is NO WARRANTY, to the extent permitted by law."
                         "\n"
                         "\nWritten by Mihai Niculescu.\n";

            return boost::none;
        }

        po::notify(vm); // throws if required vars are not set


        // everything went smooth.. lets return the parsed args
        return result;
    }
    catch(boost::program_options::required_option& e)
    {
        std::cout << "Some options that don't have a default value are REQUIRED. To see them run: " << APP_NAME <<" -h\n";
        return boost::none;
    }
    catch(boost::program_options::error& e)
    {
        std::cerr << APP_NAME << " has encountered an error.\n " << e.what() << std::endl;
        return boost::none;
    }
    catch(...) {
        std::cout << "An unknown error has occured - \n";
        throw;
    }


}

std::string Settings::basefilename() const
{
    return m_basefilename;
}

std::string Settings::datadir() const
{
    return m_datadir;
}

std::string Settings::outputdir() const
{
    return m_outputdir;
}

uint64_t Settings::ibegin() const
{
    return m_ibegin;
}

uint64_t Settings::iend() const
{
    return m_iend;
}

uint32_t Settings::njobs() const
{
    return m_njobs;
}

std::ostream &Settings::print_info(std::ostream &ost) const
{
    ost << "( Settings: "
        << "\nbasefilename: " << m_basefilename
        << "\ndatadir: " << m_datadir
        << "\noutputdir: " << m_outputdir
        << "\nconfigfile: " << m_configfile
        << "\nbegin-index: " << m_ibegin
        << "\nend-index: " << m_iend
        << "\njobs: " << m_njobs
        << "\n ) \n";

    return ost;
}

std::string Settings::convertername() const
{
    return m_convertername;
}
